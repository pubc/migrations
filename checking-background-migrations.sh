#!/bin/bash
TAG=`grep -m 1 gitlab /opt/gitlab/version-manifest.txt | awk '{print $2}'`

function ZeroFix(){
    if [[ ${#1} -eq 1 ]]; then
        echo 0${1}
    else
        echo $1
    fi
}

MAJOR=`echo $TAG|cut -d . -f 1`
MINOR=`echo $TAG|cut -d . -f 2`
PATCH=`echo $TAG|cut -d . -f 3`
TAG=$(ZeroFix $MAJOR).$(ZeroFix $MINOR).$(ZeroFix $PATCH)

function BgMigrationRemain(){
     gitlab-rails runner -e production 'puts Gitlab::BackgroundMigration.remaining'
}

echo -e "Checking remaining & pending migrations: \c"
if [[ $TAG > '15.01.00' || $TAG == '15.01.00' ]]; then
    PENDING=`BgMigrationRemain`
    BM=`gitlab-rails runner -e production 'puts Gitlab::Database::BackgroundMigration::BatchedMigration.queued.count'`
elif [[ $TAG > '14.08.00' || $TAG == '14.08.00' ]]; then
    PENDING=`BgMigrationRemain`
    BM=`gitlab-rails runner -e production 'puts Gitlab::Database::BackgroundMigrationJob.pending.count'`
elif [[ $TAG > '14.07.00' || $TAG == '14.07.00' ]]; then
    PENDING=`BgMigrationRemain`
    BM=`gitlab-rails runner -e production 'puts Gitlab::Database::BackgroundMigrationJob.pending'`
elif [[ $TAG > '14.06.00' || $TAG == '14.06.00' ]]; then
    PENDING=`BgMigrationRemain`
    BM=`gitlab-rails runner -e production 'puts Gitlab::BackgroundMigration.pending'`
elif [[ ($TAG < '14.06.00' || $TAG == '14.05.00') && ($TAG > '12.09.00' || $TAG == '12.09.00') ]]; then
    PENDING=`BgMigrationRemain`
    BM=0
else
    PENDING=`gitlab-rails c <<EOF | tail -2 | head -1
    puts "#{Sidekiq::Queue.new("background_migration").size+Sidekiq::ScheduledSet.new.select { |r| r.klass == 'BackgroundMigrationWorker' }.size}"
EOF`
    BM=0
fi
expr $PENDING + $BM

echo -e "Checking failed migrations: \c"
if [[ $TAG > '14.10.00' || $TAG == '14.10.00' ]]; then
    FAILED=`gitlab-rails runner -e production 'Gitlab::Database::BackgroundMigration::BatchedMigration.with_status(:failed).count'`
elif [[ ($TAG < '14.10.00' || $TAG == '14.09.00') && ($TAG > '14.00.00' || $TAG == '14.00.00') ]]; then
    FAILED=`gitlab-rails runner -e production 'Gitlab::Database::BackgroundMigration::BatchedMigration.failed.count'`
fi
if [[ -z $FAILED ]]; then
    echo 0
fi

echo -e "Checking batched background migrations: \c"
if [[ $TAG > '14.00.00' || $TAG == '14.00.00' ]]; then
     gitlab-psql -t -q -c 'select count(*) from batched_background_migrations where status <> 3;' | sed 's/ //g;/^$/d'
fi

echo -e "Checking elasticsearch pending migrations: \c"
    gitlab-rake gitlab:elastic:list_pending_migrations 2>/dev/null | egrep -v "There|Pending migrations" | wc -l

echo -e "Checking legacy storage projects: \c"
    gitlab-rake gitlab:storage:list_legacy_projects 2>/dev/null | awk '{print $3}'
